import { Sim } from "xiv-mech-sim";
import {
  Castbar,
  LCNumbers,
  BlueMarker,
  Success,
} from "xiv-mech-sim/info_displays.js";
import { CircleAoE } from "xiv-mech-sim/mechanics.js";
import { rotatePoint } from "xiv-mech-sim/utilities.js";
import {
  Arena,
  IceWalls,
  LevinOrb,
  LevinstrikeTower,
  Firemeld,
} from "./mechanics.js";

let partSelect = document.querySelector('select[name="part"]');

function handlePositionInputChange(evt) {
  if (evt.target.value == "None") {
    partSelect.disabled = true;
  } else {
    partSelect.disabled = false;
  }
}

for (let input of document.querySelectorAll('input[name="player"]')) {
  input.addEventListener("change", handlePositionInputChange);
}

let sim = new Sim({
  placeholderId: "sim",
  drawingSize: 50, // yalms!
  backgroundColor: "rgb(0,0,0)",
  menuId: "menu",
  initFn: function (sim) {
    rollRNG(sim);
    sim.entities.add(new Arena(sim));
    setUpMechanics(sim);
    setUpDutySupport(sim);
  },
});

sim.run();

const numbersToDirections = {
  0: "N",
  1: "NE",
  2: "E",
  3: "SE",
  4: "S",
  5: "SW",
  6: "W",
  7: "NW",
};

const playerPositionOptions = {
  any: null,
  lc: { d: false, lc: true },
  lc2: { d: false, lc: true, number: 1 },
  lc4: { d: false, lc: true, number: 2 },
  lc6: { d: false, lc: true, number: 3 },
  lc8: { d: false, lc: true, number: 4 },
  d: { d: true, lc: false },
  d1: { d: true, lc: false, number: 1 },
  d2: { d: true, lc: false, number: 2 },
  d3: { d: true, lc: false, number: 3 },
  d4: { d: true, lc: false, number: 4 },
};

let rngChoices = {};

function rollRNG(sim) {
  rngChoices.orbOnePosition = Math.floor(Math.random() * 8);
  rngChoices.clockwise = Math.random() > 0.5;
  rngChoices.numberedPartyMembers = [];
  rngChoices.defamationTargets = [];

  let playerPosition = null;
  let playerLabel = document.querySelector(
    `input[type="radio"][name="player"]:checked`,
  ).value;
  if (playerLabel != "None") {
    playerPosition = structuredClone(
      playerPositionOptions[
        document.querySelector('select[name="part"]').value
      ],
    );
    if (playerPosition && !Object.hasOwn(playerPosition, "number")) {
      playerPosition.number = 1 + Math.floor(Math.random() * 4);
    }
  }

  let toShuffle = sim.party.list.slice();
  let player = null;
  if (playerPosition) {
    for (let i = 0; i < 8; i++) {
      if (toShuffle[i].label == playerLabel) {
        player = toShuffle[i];
        toShuffle.splice(i, 1);
        break;
      }
    }
  }
  for (let i = 1; i <= 4; i++) {
    if (playerPosition && playerPosition.lc && playerPosition.number == i) {
      rngChoices.numberedPartyMembers.push(player);
    } else {
      let selected = Math.floor(Math.random() * toShuffle.length);
      rngChoices.numberedPartyMembers.push(toShuffle[selected]);
      toShuffle.splice(selected, 1);
    }
    if (playerPosition && playerPosition.d && playerPosition.number == i) {
      rngChoices.defamationTargets.push(player);
    } else {
      let selected = Math.floor(Math.random() * toShuffle.length);
      rngChoices.defamationTargets.push(toShuffle[selected]);
      toShuffle.splice(selected, 1);
    }
  }

  console.log(JSON.stringify(rngChoices));
}

// all times are in milliseconds
const levinstrike_castbar_appears = 1700;
const levinstrike_castbar_duration = 3700;
const orbs_appear = 6000;
const scrambled_castbar_appears = 7000;
const scrambled_castbar_duration = 9700;
const limit_cut_numbers_appear = 7200;
const limit_cut_numbers_disappear = 16000;

// per-orb events & delay between orbs
const defamation_marker_one_appears = 13500;
const orb_one_moves = 20000;
const orb_one_explodes = 21000;
const orb_one_tower_appears = 22000;
const first_numbered_player_hit = 23000; // firemeld
const first_defamation_explodes = 23200; // icemeld
const first_tower_explodes = 23400; // shock
const delay_between_orbs = 5500;

const succeed = first_tower_explodes + 3 * delay_between_orbs + 1000;

const hit_effect_duration = 500;

function setUpMechanics(sim) {
  let ice_walls = new IceWalls(rngChoices.orbOnePosition % 2 != 0);
  sim.entities.add(ice_walls);
  ice_walls.hit([]);

  let levinstrike_castbar = new Castbar(
    "Levinstrike Summoning",
    levinstrike_castbar_duration,
  );
  sim.timeline.addEvent(levinstrike_castbar_appears, () => {
    sim.entities.add(levinstrike_castbar);
  });
  sim.timeline.addEvent(
    levinstrike_castbar_appears + levinstrike_castbar_duration,
    () => {
      sim.entities.remove(levinstrike_castbar);
    },
  );

  let orbs = [];
  let nextOrb = rngChoices.clockwise ? 2 : -2;
  orbs.push(new LevinOrb(rngChoices.orbOnePosition));
  orbs.push(new LevinOrb((rngChoices.orbOnePosition + nextOrb + 8) % 8));
  orbs.push(new LevinOrb((rngChoices.orbOnePosition + 2 * nextOrb + 8) % 8));
  orbs.push(new LevinOrb((rngChoices.orbOnePosition + 3 * nextOrb + 8) % 8));

  sim.timeline.addEvent(orbs_appear, () => {
    for (let o of orbs) {
      sim.entities.add(o);
    }
  });

  let scrambled_castbar = new Castbar(
    "Scrambled Succession",
    scrambled_castbar_duration,
  );
  sim.timeline.addEvent(scrambled_castbar_appears, () => {
    sim.entities.add(scrambled_castbar);
  });
  sim.timeline.addEvent(
    scrambled_castbar_appears + scrambled_castbar_duration,
    () => {
      sim.entities.remove(scrambled_castbar);
    },
  );

  let lc_numbers_map = new Map();
  lc_numbers_map.set(orbs[0], 1);
  lc_numbers_map.set(rngChoices.numberedPartyMembers[0], 2);
  lc_numbers_map.set(orbs[1], 3);
  lc_numbers_map.set(rngChoices.numberedPartyMembers[1], 4);
  lc_numbers_map.set(orbs[2], 5);
  lc_numbers_map.set(rngChoices.numberedPartyMembers[2], 6);
  lc_numbers_map.set(orbs[3], 7);
  lc_numbers_map.set(rngChoices.numberedPartyMembers[3], 8);
  let lc_numbers = new LCNumbers(lc_numbers_map);
  sim.timeline.addEvent(limit_cut_numbers_appear, () => {
    sim.entities.add(lc_numbers);
  });
  sim.timeline.addEvent(limit_cut_numbers_disappear, () => {
    sim.entities.remove(lc_numbers);
  });

  for (let i = 0; i < 4; i++) {
    let tower = new LevinstrikeTower(
      orbs[i].endPosition,
      rngChoices.numberedPartyMembers[(i + 2) % 4],
    );
    let lc_hit = new Firemeld(rngChoices.numberedPartyMembers[i], {
      x: orbs[i].x,
      y: orbs[i].y,
    });
    let defamation_marker = new BlueMarker(rngChoices.defamationTargets[i]);
    let defamation = new CircleAoE({
      target: rngChoices.defamationTargets[i],
      radius: 20,
      hit_color: "rgba(245,245,245,0.8)",
    });
    sim.timeline.addEvent(
      defamation_marker_one_appears + i * delay_between_orbs,
      () => {
        sim.entities.add(defamation_marker);
      },
    );
    sim.timeline.addEvent(orb_one_moves + i * delay_between_orbs, () => {
      orbs[i].go();
    });
    sim.timeline.addEvent(orb_one_explodes + i * delay_between_orbs, () => {
      orbs[i].hit(sim.party.list);
    });
    sim.timeline.addEvent(
      orb_one_tower_appears + i * delay_between_orbs,
      () => {
        sim.entities.remove(orbs[i]);
        sim.entities.add(tower);
      },
    );
    sim.timeline.addEvent(
      first_numbered_player_hit + i * delay_between_orbs,
      () => {
        sim.entities.add(lc_hit);
        lc_hit.hit(sim.party.list);
      },
    );
    sim.timeline.addEvent(
      first_numbered_player_hit + i * delay_between_orbs + hit_effect_duration,
      () => {
        sim.entities.remove(lc_hit);
      },
    );
    sim.timeline.addEvent(
      first_defamation_explodes + i * delay_between_orbs,
      () => {
        sim.entities.remove(defamation_marker);
        sim.entities.add(defamation);
        defamation.hit(sim.party.list);
      },
    );
    sim.timeline.addEvent(
      first_defamation_explodes + i * delay_between_orbs + hit_effect_duration,
      () => {
        sim.entities.remove(defamation);
      },
    );
    sim.timeline.addEvent(first_tower_explodes + i * delay_between_orbs, () => {
      tower.hit(sim.party.list);
    });
    sim.timeline.addEvent(
      first_tower_explodes + i * delay_between_orbs + hit_effect_duration,
      () => {
        sim.entities.remove(tower);
      },
    );
  }

  sim.timeline.addEvent(succeed, () => {
    sim.entities.add(new Success());
    sim.endRun();
  });
}

const thinking_delay = 1500;
const reaction_delay = 500;

const initial_positioning = limit_cut_numbers_appear + thinking_delay;
const first_defamation_positioning =
  defamation_marker_one_appears + thinking_delay;
const lc_target_return = first_numbered_player_hit + reaction_delay;
const defamation_targets_move =
  first_defamation_explodes - delay_between_orbs + reaction_delay;
const tower_soaker_return = first_tower_explodes + reaction_delay;

function setUpDutySupport(sim) {
  let party = sim.party.list;

  // randomize starting positions
  for (let pm of party) {
    pm.set_position(blur(100, 5), blur(100, 5));
  }

  let strat = document.querySelector('input[name="strat"]:checked').value;
  let strat_positions = [{}, {}, {}, {}];
  let strat_timings = {};
  strat_positions[0].tower = rotatePoint(
    { x: 100, y: 118 },
    45 * rngChoices.orbOnePosition,
  );
  let rotationIterator = rngChoices.clockwise ? 90 : -90;
  for (let i = 1; i < 4; i++) {
    strat_positions[i].tower = rotatePoint(
      strat_positions[i - 1].tower,
      rotationIterator,
    );
  }
  if (strat == "JP") {
    let initial_position_rotation =
      45 * (rngChoices.orbOnePosition + (rngChoices.clockwise ? 1 : -1));
    strat_positions[0].main = strat_positions[1].main = rotatePoint(
      { x: 100, y: 106 },
      initial_position_rotation,
    );
    strat_positions.oppo_start =
      strat_positions[0].oppo =
      strat_positions[1].oppo =
        strat_positions[0].main;
    strat_positions[0].lc = strat_positions[1].lc = rotatePoint(
      { x: 100, y: 119 },
      initial_position_rotation,
    );
    strat_positions[0].defamation = strat_positions[1].defamation = rotatePoint(
      { x: 100, y: 81 },
      initial_position_rotation,
    );
    strat_positions[2].main = strat_positions[3].main = rotatePoint(
      strat_positions[0].main,
      180,
    );
    strat_positions[2].oppo = strat_positions[3].oppo = strat_positions[2].main;
    strat_positions[2].lc = strat_positions[3].lc = rotatePoint(
      strat_positions[0].lc,
      180,
    );
    strat_positions[2].defamation = strat_positions[3].defamation = rotatePoint(
      strat_positions[0].defamation,
      180,
    );

    strat_timings.tower_soaker_go = orb_one_moves + reaction_delay;
    strat_timings.lc_target_go = strat_timings.tower_soaker_go;
    // main group moves before anything actually happens
    strat_timings.main_group_go =
      first_defamation_explodes - delay_between_orbs + reaction_delay;
    strat_timings.oppo_group_go = strat_timings.main_group_go;
  } else {
    // strat is oppo or Krile, which share all but oppo positions
    strat_positions[0].main = { x: 100, y: 108 };
    if (strat == "oppo") {
      strat_positions.oppo_start = { x: 100, y: 83 };
      strat_positions[0].oppo = { x: 115, y: 100 };
    } else {
      strat_positions.oppo_start = strat_positions[0].oppo = structuredClone(
        strat_positions[0].main,
      );
    }
    strat_positions[0].lc = { x: 113, y: 113 };
    strat_positions[0].defamation = { x: 100, y: 83 };
    strat_positions[0].tower = { x: 100, y: 117 };

    let initial_position_rotation = 45 * rngChoices.orbOnePosition;
    let rotation_iteration = rngChoices.clockwise ? 90 : -90;

    strat_positions.oppo_start = rotatePoint(
      strat_positions.oppo_start,
      initial_position_rotation,
    );

    for (let positionName of Object.getOwnPropertyNames(strat_positions[0])) {
      strat_positions[0][positionName] = rotatePoint(
        strat_positions[0][positionName],
        initial_position_rotation,
      );
      for (let i = 1; i < 4; i++) {
        strat_positions[i][positionName] = rotatePoint(
          strat_positions[i - 1][positionName],
          rotation_iteration,
        );
      }
    }

    strat_timings.tower_soaker_go = orb_one_moves + reaction_delay + 1000;
    // main group moves before anything actually happens
    strat_timings.main_group_go =
      first_defamation_explodes - delay_between_orbs + reaction_delay;
    strat_timings.lc_target_go = strat_timings.main_group_go + 100; // should fix so they can go at the same time but lazy
    if (strat == "oppo") {
      strat_timings.oppo_group_go = defamation_targets_move;
    } else {
      strat_timings.oppo_group_go = strat_timings.main_group_go;
    }
  }

  sim.timeline.addEvent(initial_positioning, () => {
    rngChoices.numberedPartyMembers[0].set_target_positions([
      strat_positions[0].lc,
    ]);
    for (let i = 1; i < 4; i++) {
      rngChoices.numberedPartyMembers[i].set_target_positions([
        {
          x: blur(strat_positions[0].main.x, 1),
          y: blur(strat_positions[0].main.y, 1),
        },
      ]);
    }
    for (let pm of rngChoices.defamationTargets) {
      pm.set_target_positions([
        {
          x: blur(strat_positions.oppo_start.x, 1),
          y: blur(strat_positions.oppo_start.y, 1),
        },
      ]);
    }
  });

  sim.timeline.addEvent(first_defamation_positioning, () => {
    rngChoices.defamationTargets[0].set_target_positions([
      strat_positions[0].defamation,
    ]);
  });

  for (let i = 0; i < 4; i++) {
    let tower_soaker = rngChoices.numberedPartyMembers[(i + 2) % 4];
    let lc_target = rngChoices.numberedPartyMembers[i];
    let defamation_target = rngChoices.defamationTargets[i];
    // leave out previous orb's lc and tower targets because
    // their position assignments likely need to be delayed.
    // but this orb's targets can move with the group first.
    let main_group = [tower_soaker, lc_target];
    for (let j = 0; j < i; j++) {
      main_group.push(rngChoices.defamationTargets[j]);
    }
    let oppo_group = [];
    for (let j = i + 1; j < 4; j++) {
      oppo_group.push(rngChoices.defamationTargets[j]);
    }

    if (i > 0) {
      sim.timeline.addEvent(
        defamation_targets_move + i * delay_between_orbs,
        () => {
          console.log("move current & previous defamation targets");
          defamation_target.set_target_positions([
            strat_positions[i].defamation,
          ]);
          rngChoices.defamationTargets[i - 1].set_target_positions([
            strat_positions[i].main,
          ]);
        },
      );

      sim.timeline.addEvent(
        strat_timings.lc_target_go + i * delay_between_orbs,
        () => {
          lc_target.set_target_positions([strat_positions[i].lc]);
        },
      );

      sim.timeline.addEvent(
        strat_timings.main_group_go + i * delay_between_orbs,
        () => {
          if (strat_positions[i].main != strat_positions[i - 1].main) {
            for (let pm of main_group) {
              pm.set_target_positions([
                {
                  x: blur(strat_positions[i].main.x, 1),
                  y: blur(strat_positions[i].main.y, 1),
                },
              ]);
            }
          }
        },
      );
    }
    sim.timeline.addEvent(
      strat_timings.oppo_group_go + i * delay_between_orbs,
      () => {
        console.log("start oppo group moving");
        if (i == 0 || strat_positions[i].oppo != strat_positions[i - 1].oppo) {
          for (let pm of oppo_group) {
            pm.set_target_positions([
              {
                x: blur(strat_positions[i].oppo.x, 1),
                y: blur(strat_positions[i].oppo.y, 1),
              },
            ]);
          }
        }
      },
    );
    let towerPosition = rotatePoint(
      { x: 100, y: 116.5 },
      rngChoices.orbOnePosition * 45 + i * rotationIterator,
    );
    sim.timeline.addEvent(
      strat_timings.tower_soaker_go + i * delay_between_orbs,
      () => {
        tower_soaker.set_target_positions([towerPosition]);
      },
    );
    if (i < 3) {
      sim.timeline.addEvent(lc_target_return + i * delay_between_orbs, () => {
        lc_target.set_target_positions([strat_positions[i + 1].main]);
      });
      sim.timeline.addEvent(
        tower_soaker_return + i * delay_between_orbs,
        () => {
          tower_soaker.set_target_positions([strat_positions[i + 1].main]);
        },
      );
    }
  }
}

function blur(number, max_amount) {
  return number - max_amount + 2 * max_amount * Math.random();
}
