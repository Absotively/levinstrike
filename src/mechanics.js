import { Entity } from "xiv-mech-sim/entities.js";
import { RoundArena } from "xiv-mech-sim/arena.js";
import { createSVGElement } from "xiv-mech-sim/drawing.js";
import { standardWarningColor } from "xiv-mech-sim/constants.js";
import { rotatePoint, distance } from "xiv-mech-sim/utilities.js";
import { CircleAoE } from "xiv-mech-sim/mechanics.js";

class Arena extends RoundArena {
  #svgGroup;

  constructor(sim) {
    super(sim, 40, "#4a7595");
    this.#svgGroup = null;
  }

  addToDrawing(drawing) {
    super.addToDrawing(drawing);
    this.#svgGroup = createSVGElement("g");
    let linesOnTheFloor = createSVGElement("path");
    linesOnTheFloor.setAttribute(
      "d",
      "M100,80 L100,120 M80,100 L120,100 " +
        "M85.86,85.86 L114.14,114.14 M85.86,114.14 L114.14 85.86 " +
        "M100,92 a8 8 180 1 1 0,16 a8 8 180 1 1 0,-16 " +
        "M100,86 a14 14 180 1 1 0,28 a14 14 180 1 1 0,-28",
    );
    linesOnTheFloor.setAttribute("stroke-width", "0.1");
    linesOnTheFloor.setAttribute("stroke", "#bff4ff");
    linesOnTheFloor.setAttribute("fill", "none");
    this.#svgGroup.append(linesOnTheFloor);
    let pinkWallOfDeath = createSVGElement("circle");
    pinkWallOfDeath.setAttribute("cx", "100");
    pinkWallOfDeath.setAttribute("cy", "100");
    pinkWallOfDeath.setAttribute("r", "21");
    pinkWallOfDeath.setAttribute("stroke-width", "2");
    pinkWallOfDeath.setAttribute("stroke", "#c033f5");
    pinkWallOfDeath.setAttribute("fill", "none");
    this.#svgGroup.append(pinkWallOfDeath);
    let higherContrastDeathWallRing = pinkWallOfDeath.cloneNode();
    higherContrastDeathWallRing.setAttribute("r", "20.1");
    higherContrastDeathWallRing.setAttribute("stroke-width", "0.2");
    higherContrastDeathWallRing.setAttribute("stroke", "#ffefff");
    this.#svgGroup.append(higherContrastDeathWallRing);
    drawing.layers.arena.append(this.#svgGroup);
  }

  removeFromDrawing() {
    this.#svgGroup.remove();
    this.#svgGroup = null;
    super.removeFromDrawing();
  }
}

const IceWallPathSpec = "M92.35,81.52 a20 20 22.5 0 1 15.3,0 l0,1 -15.3,0 z";

class IceWall extends Entity {
  #direction;
  #path;
  has_hit;

  constructor(direction) {
    super();
    this.#direction = direction;
    this.has_hit = false;
  }

  addToDrawing(drawing) {
    this.#path = createSVGElement("path");
    this.#path.setAttribute("d", IceWallPathSpec);
    if (this.#direction != 0) {
      this.#path.setAttribute(
        "transform",
        `rotate(${this.#direction * 45} 100 100)`,
      );
    }
    if (this.has_hit) {
      this.#path.setAttribute("fill", "#9af1ff");
    } else {
      this.#path.setAttribute("fill", standardWarningColor);
    }
    drawing.layers.unclippedMechanics.append(this.#path);
  }

  removeFromDrawing() {
    this.#path.remove();
    this.#path = null;
  }

  hit(partyList) {
    for (let pm of partyList) {
      if (this.#containsPoint(pm.x, pm.y)) {
        pm.ko();
      }
    }
    this.#path.setAttribute("fill", "#9af1ff");
    this.has_hit = true;
  }

  allowMovement(from, to) {
    if (this.has_hit && this.#containsPoint(to.x, to.y)) {
      return false;
    } else {
      return true;
    }
  }

  #containsPoint(x, y) {
    // rotate point and wall around 100, 100 to put this wall north
    // (wall rotation not actually needed, will just compare to where
    // the wall would be if it were north)
    let rotated = rotatePoint({ x: x, y: y }, -45 * this.#direction);
    if (rotated.y < 82.52 && rotated.x > 92.35 && rotated.x < 107.65) {
      // don't care if it's actually past the wall of death
      return true;
    }
    return false;
  }
}

class IceWalls extends Entity {
  #walls;

  constructor(intercards) {
    super();
    let directions = intercards ? [1, 3, 5, 7] : [0, 2, 4, 6];
    this.#walls = [];
    for (let d of directions) {
      this.#walls.push(new IceWall(d));
    }
  }

  addToDrawing(drawing) {
    for (let w of this.#walls) {
      w.addToDrawing(drawing);
    }
  }

  removeFromDrawing() {
    for (let w of this.#walls) {
      w.removeFromDrawing();
    }
  }

  hit(partyList) {
    for (let w of this.#walls) {
      w.hit(partyList);
    }
  }

  allowMovement(from, to) {
    for (let w of this.#walls) {
      if (!w.allowMovement(from, to)) {
        return false;
      }
    }
    return true;
  }
}

const diagonalMovementVectorComponent = 1 / Math.sqrt(2);

const orbStartDirectionToMovementVectors = {
  0: { x: 0, y: 1 },
  1: {
    x: -1 * diagonalMovementVectorComponent,
    y: diagonalMovementVectorComponent,
  },
  2: { x: -1, y: 0 },
  3: {
    x: -1 * diagonalMovementVectorComponent,
    y: -1 * diagonalMovementVectorComponent,
  },
  4: { x: 0, y: -1 },
  5: {
    x: diagonalMovementVectorComponent,
    y: -1 * diagonalMovementVectorComponent,
  },
  6: { x: 1, y: 0 },
  7: { x: diagonalMovementVectorComponent, y: diagonalMovementVectorComponent },
};

class LevinOrb extends Entity {
  #circle;
  constructor(direction) {
    super();
    this.startDirection = direction;
    let startPosition = rotatePoint({ x: 100, y: 89 }, direction * 45);
    this.x = startPosition.x;
    this.y = startPosition.y;
    this.endPosition = rotatePoint({ x: 100, y: 116.5 }, direction * 45);
    this.unitMovementVector = { x: 0, y: 0 };
  }

  addToDrawing(drawing) {
    this.#circle = createSVGElement("circle");
    this.#circle.setAttribute("cx", `${this.x}`);
    this.#circle.setAttribute("cy", `${this.y}`);
    this.#circle.setAttribute("r", "1.25");
    this.#circle.setAttribute("fill", "#d892f7");
    this.#circle.setAttribute("stroke-width", "1.5");
    this.#circle.setAttribute("stroke", "#493590");
    drawing.layers.arenaClippedMechanics.append(this.#circle);
  }

  go() {
    this.speed = 27.5; // yalms per second
    this.unitMovementVector =
      orbStartDirectionToMovementVectors[this.startDirection];
  }

  update(sim, msSinceLastUpdate) {
    if (this.unitMovementVector.x != 0 || this.unitMovementVector.y != 0) {
      let movementDistance = (this.speed * msSinceLastUpdate) / 1000;
      if (distance(this, this.endPosition) <= movementDistance) {
        this.x = this.endPosition.x;
        this.y = this.endPosition.y;
        this.unitMovementVector = { x: 0, y: 0 };
      } else {
        this.x += movementDistance * this.unitMovementVector.x;
        this.y += movementDistance * this.unitMovementVector.y;
      }
      this.#circle.setAttribute("cx", `${this.x}`);
      this.#circle.setAttribute("cy", `${this.y}`);
    }
  }

  hit(partyList) {
    for (let pm of partyList) {
      if (distance(pm, this) < 6) {
        pm.ko();
      }
    }
    this.#circle.setAttribute("r", "6");
    this.#circle.setAttribute("fill", "rgba(81, 42, 172, 0.3)");
    this.#circle.setAttribute("stroke", "none");
  }

  removeFromDrawing() {
    this.#circle.remove();
    this.#circle = null;
  }
}

class LevinstrikeTower extends Entity {
  #svgGroup;

  constructor(position, assignedSoaker) {
    super();
    this.position = position;
    this.assignedSoaker = assignedSoaker;
    this.radius = 3.5;
    this.has_hit = false;
    this.#svgGroup = null;
  }

  hit(partyList) {
    let in_tower_count = 0;
    for (let pm of partyList) {
      if (distance(pm, this.position) < this.radius) {
        in_tower_count++;
        if (!Object.is(pm, this.assignedSoaker)) {
          pm.ko();
        }
      }
    }
    if (in_tower_count == 0) {
      for (let pm of partyList) {
        pm.ko();
      }
    }
    this.has_hit = true;
    let hit_effect = createSVGElement("rect");
    hit_effect.setAttribute("x", "79");
    hit_effect.setAttribute("y", "79");
    hit_effect.setAttribute("width", "42");
    hit_effect.setAttribute("height", "42");
    hit_effect.setAttribute("fill", "rgba(5,5,5,0.1)");
    this.#svgGroup.replaceChildren(hit_effect);
  }

  addToDrawing(drawing) {
    if (this.has_hit) {
      console.log("oops");
    }
    this.#svgGroup = createSVGElement("g");
    let base = createSVGElement("circle");
    base.setAttribute("cx", `${this.position.x}`);
    base.setAttribute("cy", `${this.position.y}`);
    base.setAttribute("r", `${this.radius}`);
    base.setAttribute("stroke-width", "0.3");
    base.setAttribute("stroke", "#fff");
    base.setAttribute("fill", "rgba(5,5,5,0.4)");
    this.#svgGroup.append(base);
    let tower = createSVGElement("path");
    tower.setAttribute(
      "d",
      `M${this.position.x}, ${this.position.y - 0.5} l0.5,0.85 -1,0 z`,
    );
    tower.setAttribute("fill", "#000");
    this.#svgGroup.append(tower);
    drawing.layers.arenaClippedMechanics.append(this.#svgGroup);
  }

  removeFromDrawing() {
    if (this.#svgGroup) {
      this.#svgGroup.remove();
      this.#svgGroup = null;
    }
  }
}

class Firemeld extends Entity {
  #aoe;
  #target;
  #orbStartPosition;

  constructor(target, orbStartPosition) {
    super();
    this.#aoe = new CircleAoE({
      target: target,
      radius: 6,
      hit_color: "rgba(252,233,58, 0.8)",
    });
    this.#target = target;
    this.#orbStartPosition = orbStartPosition;
  }

  addToDrawing(drawing) {
    this.#aoe.addToDrawing(drawing);
  }

  removeFromDrawing() {
    this.#aoe.removeFromDrawing();
  }

  update() {
    this.#aoe.update();
  }

  hit(partyList) {
    this.#aoe.hit(partyList);
    if (distance(this.#target, this.#orbStartPosition) < 20) {
      this.#target.ko();
    }
  }
}

export { Arena, IceWalls, LevinOrb, LevinstrikeTower, Firemeld };
