A simple sim for the Levinstrike mechanic from the fight Anabaseios: The Ninth Circle in Final Fantasy XIV.

To start a development server, run `npm start`. For deployment, run `npx rollup -c` and then serve the resulting 'dist' directory as a static page.
